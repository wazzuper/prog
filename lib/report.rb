class Report

  def full_report(dir_name, hash_with_duplicates)
    puts '##########################################'
    puts "Текущая папка: #{Dir.getwd}"
    puts "Проверяемая папка: #{dir_name}"

    count_files(dir_name)
    print_each_file(hash_with_duplicates)

    puts '##########################################'
  end

  def count_files(dir_name)
    count = 0
    unique = 0

    Dir.entries(dir_name).each do |file_name|      
      unless file_name =~ /^\.\.?$/
        count += 1

        if !file_name.include?('copy')
          unique += 1
        end
      end
    end

    puts "Всего файлов в папке: #{count}"
    puts "Уникальных файлов: #{unique}"
  end

  def print_each_file(hash_with_duplicates)
    puts 'Список файлов:'
    puts '--------------'

    hash_with_duplicates.each do |file_name, copies|
      puts "Файл #{file_name} - всего копий #{copies}" if copies > 1
      puts "Файл #{file_name} - копий нет" if copies == 1
    end

    puts '--------------'
  end
end