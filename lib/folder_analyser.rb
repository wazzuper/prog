require 'digest'

class FolderAnalyser

  def analysis(dir_name, hash_builder)
    dir_exist?(dir_name)
    hash_builder.file_names_and_digits = collect_names_and_digits_of_files(dir_name)
  end

  private

  def dir_exist?(dir_name)
    if !File.exist?(dir_name)
      puts 'Вы ввели название несуществующей папки'
      puts 'Список всех файлов и папок в директории, в которой вы находитесь:'

      puts '===================='
      all_files = Dir.getwd
      puts Dir.entries(all_files)
      puts '===================='

      exit
    end
  end

  def collect_names_and_digits_of_files(dir_name)
    file_names_and_digits = {}

    Dir.foreach(dir_name) do |file_name|
      file_names_and_digits[file_name] = Digest::SHA256.base64digest(File.read("#{dir_name}/#{file_name}")) if !File.directory?("#{dir_name}/#{file_name}")
    end

    file_names_and_digits
  end
end