require_relative 'program'

folder_analysis = FolderAnalyser.new
hash_with_file_names_and_digits = HashBuilder.new
hash_with_duplicates = DuplicatesAnalyser.new
report = Report.new

folder_analysis.analysis('somedir', hash_with_file_names_and_digits)
final_hash = hash_with_duplicates.duplicates_analysis(hash_with_file_names_and_digits)
report.full_report('somedir', final_hash)