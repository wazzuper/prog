class DuplicatesAnalyser

  def duplicates_analysis(hash_with_file_names_and_digits)
    hash_with_same_digits = count_same_digits(hash_with_file_names_and_digits)
    result = count_duplicates(hash_with_same_digits, hash_with_file_names_and_digits)
  end

  private

  def count_same_digits(hash_with_file_names_and_digits)
    hash_with_file_names_and_digits.file_names_and_digits.values.each_with_object(Hash.new(0)) { |digits, hash| hash[digits] += 1 }
  end

  def count_duplicates(hash_with_same_digits, hash_with_file_names_and_digits)
    result = {}

    hash_with_file_names_and_digits.file_names_and_digits.each do |file_name, digit|
      hash_with_same_digits.each do |name_of_digit, num|
        if digit == name_of_digit && !file_name.include?('copy')
          result[file_name] = num
        end
      end
    end

    result
  end
end