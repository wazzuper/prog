require 'digest'

class ScanFiles
  def scan(dir_name, report_files)
    dir_exist?(dir_name)
    report_files.file_names_and_digits = collect_names_and_digits_of_files(dir_name, report_files)
  end

  private

  def dir_exist?(dir_name)
    abort 'Вы ввели название несуществующей папки' unless File.exist?(dir_name)
  end

  def collect_names_and_digits_of_files(dir_name, report_files)
    file_names_and_digits = {}

    Dir.new(dir_name).each do |files_name|
      file_names_and_digits[files_name] = Digest::SHA256.base64digest(File.read("#{dir_name}/#{files_name}")) unless File.directory?("#{dir_name}/#{files_name}")
    end

    file_names_and_digits
  end
end

class ReportFiles
  attr_accessor :file_names_and_digits

  def initialize
    file_names_and_digits = {}
  end

  def report
    hash_with_same_digits = count_same_digits
    final_result = count_duplicates(hash_with_same_digits)
    puts "Наш результат: #{final_result}"
  end

  private

  def count_same_digits
    file_names_and_digits.values.each_with_object(Hash.new(0)) { |num, hash| hash[num] += 1 }
  end

  def count_duplicates(digits_hash)
    result = {}

    file_names_and_digits.each do |name, digit|
      digits_hash.each do |name_of_digit, num|
        if digit == name_of_digit && !name.include?('copy')
          result[name] = num
        end
      end
    end

    result
  end
end